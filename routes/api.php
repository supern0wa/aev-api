<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$app->get('/', function () use ($app) {
	return $app->version();
});

// User
$app->group(['prefix' => 'users'], function () use ($app) {
	$app->get('/', ['uses' => 'UserController@index']); // List
	$app->post('/', ['uses' => 'UserController@store']); // Add
	$app->get('/search', ['uses' => 'UserController@search']); // Search
	$app->get('/{id}', ['uses' => 'UserController@show']); // Show
	$app->put('/{id}', ['uses' => 'UserController@update']); // Update
	$app->delete('/{id}', ['uses' => 'UserController@destroy']); // Delete
});

// Password
$app->group(['prefix' => 'password'], function () use ($app) {
	$app->post('email', 'PasswordController@postEmail');
	$app->post('reset', 'PasswordController@postReset');
});

// Location
$app->group(['prefix' => 'location'], function () use ($app) {
	$app->get('/countries', ['uses' => 'LocationController@index']); // List
	$app->get('/cep', ['uses' => 'LocationController@cep']); // Show
});    

// Institution
$app->group(['prefix' => 'institutions'], function () use ($app) {
	$app->get('/', ['uses' => 'InstitutionController@index']); // List
	$app->post('/', ['uses' => 'InstitutionController@store']); // Add
	$app->get('/{id}', ['uses' => 'InstitutionController@show']); // Show
	$app->put('/{id}', ['uses' => 'InstitutionController@update']); // Update
	$app->delete('/{id}', ['uses' => 'InstitutionController@destroy']); // Delete
});

// Donation
$app->group(['prefix' => 'donations'], function () use ($app) {
	$app->get('/', ['uses' => 'DonationController@index']); // List
	$app->post('/', ['uses' => 'DonationController@store']); // Add
	$app->get('/search', ['uses' => 'DonationController@search']); // Search
	$app->get('/{id}', ['uses' => 'DonationController@show']); // Show
	$app->put('/{id}', ['uses' => 'DonationController@update']); // Update
	$app->delete('/{id}', ['uses' => 'DonationController@destroy']); // Delete
});

// Category
$app->group(['prefix' => 'categories'], function () use ($app) {
	$app->get('/', ['uses' => 'CategoryController@index']); // List
	$app->post('/', ['uses' => 'CategoryController@store']); // Add
	$app->get('/{id}', ['uses' => 'CategoryController@show']); // Show
	$app->put('/{id}', ['uses' => 'CategoryController@update']); // Update
	$app->delete('/{id}', ['uses' => 'CategoryController@destroy']); // Delete
});   

// Post
$app->group(['prefix' => 'posts'], function () use ($app) {
	$app->get('/', ['uses' => 'PostController@index']); // List
	$app->post('/', ['uses' => 'PostController@store']); // Add
	$app->get('/search', ['uses' => 'PostController@search']); // Search
	$app->get('/{id}', ['uses' => 'PostController@show']); // Show
	$app->put('/{id}', ['uses' => 'PostController@update']); // Update
	$app->delete('/{id}', ['uses' => 'PostController@destroy']); // Delete
});

// Features
$app->group(['prefix' => 'features'], function () use ($app) {
	$app->get('/', ['uses' => 'FeatureController@index']); // List
	$app->post('/', ['uses' => 'FeatureController@store']); // Add
	$app->get('/search', ['uses' => 'FeatureController@search']); // Search
	$app->get('/{id}', ['uses' => 'FeatureController@show']); // Show
	$app->put('/{id}', ['uses' => 'FeatureController@update']); // Update
	$app->delete('/{id}', ['uses' => 'FeatureController@destroy']); // Delete
});

// Page
$app->group(['prefix' => 'pages'], function () use ($app) {
	$app->get('/', ['uses' => 'PageController@index']); // List
	$app->post('/', ['uses' => 'PageController@store']); // Add
	$app->get('/search', ['uses' => 'PageController@search']); // Search
	$app->get('/{id}', ['uses' => 'PageController@show']); // Show
	$app->put('/{id}', ['uses' => 'PageController@update']); // Update
	$app->delete('/{id}', ['uses' => 'PageController@destroy']); // Delete
});

// Event
$app->group(['prefix' => 'events'], function () use ($app) {
	$app->get('/', ['uses' => 'EventController@index']); // List
	$app->post('/', ['uses' => 'EventController@store']); // Add
	$app->get('/search', ['uses' => 'EventController@search']); // Search
	$app->get('/{id}', ['uses' => 'EventController@show']); // Show
	$app->put('/{id}', ['uses' => 'EventController@update']); // Update
	$app->delete('/{id}', ['uses' => 'EventController@destroy']); // Delete
});

// Partner
$app->group(['prefix' => 'partners'], function () use ($app) {
	$app->get('/', ['uses' => 'PartnerController@index']); // List
	$app->post('/', ['uses' => 'PartnerController@store']); // Add
	$app->get('/search', ['uses' => 'PartnerController@search']); // Search
	$app->get('/{id}', ['uses' => 'PartnerController@show']); // Show
	$app->put('/{id}', ['uses' => 'PartnerController@update']); // Update
	$app->delete('/{id}', ['uses' => 'PartnerController@destroy']); // Delete
});

// Volunteer
$app->group(['prefix' => 'volunteers'], function () use ($app) {
	$app->get('/', ['uses' => 'VolunteerController@index']); // List
	$app->post('/', ['uses' => 'VolunteerController@store']); // Add
	$app->get('/search', ['uses' => 'VolunteerController@search']); // Search
	$app->get('/{id}', ['uses' => 'VolunteerController@show']); // Show
	$app->put('/{id}', ['uses' => 'VolunteerController@update']); // Update
	$app->delete('/{id}', ['uses' => 'VolunteerController@destroy']); // Delete
});  

//Medias
$app->group(['prefix' => 'medias'], function () use ($app) {
	$app->post('/', ['uses' => 'MediaController@store']);
	$app->get('/search', ['uses' => 'MediaController@search']); // Search
	$app->get('/list', ['uses' => 'MediaController@getFileList']);
	$app->put('/{id}', ['uses' => 'MediaController@update']); // Update
	$app->get('/images', ['uses' => 'MediaController@images']);
	$app->get('/files', ['uses' => 'MediaController@files']);
	$app->get('/{id}', ['uses' => 'MediaController@show']);
	$app->delete('/{id}', ['uses' => 'MediaController@destroy']);
});

//Payment
$app->group(['prefix' => 'payments'], function () use ($app) {
  $app->group(['prefix' => 'notification'], function () use ($app) {
	$app->post('/{gateway}', ['uses' => 'PaymentController@notification']); // Pagseguro Notification
  });
});
