<?php

namespace App\Events;

use App\Models\Donation;
use Illuminate\Queue\SerializesModels;

class DonationCreated
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(Donation $donation)
    {
        $invoice = $donation->invoice;

        // Updates invoice total
        if ($invoice) {
            $invoice->updateTotal($donation->getPrice());
            $invoice->save();
        }
    }
}
