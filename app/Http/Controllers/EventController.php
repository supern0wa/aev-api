<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;


class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $events = Event::with('institution', 'thumbnail', 'gallery')->paginate((int) $request->input('limit', 10))
                    ->appends($request->all());
        return response($events, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id)
    {
        $event = Event::with('institution', 'thumbnail','gallery')->find($id);
        return response($event, 200);
    }
    
    /**
     *  Search Users
     * @param Request $request
     * @return type
     */
    
    public function search(Request $request)
    {
        $input = $request->all();
        try {
        $events = Event::with('institution', 'thumbnail','gallery')
            ->search($input['q'])
            ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            $e->getMessage();
            return response($e->getMessage(), 400);
        }

        return response($events, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Institution
     */
    public function store(Request $request)
    {
	
	$this->validate($request, Event::$validations);
        $input = $request->input('event');
	$gallery = $request->input('gallery');
        $event = Event::create($input);
	
	$medias = array();
        
        foreach($gallery as $index => $item):
	    $medias[$item['media_id']] = ['order' => $index, 'caption' => $item['caption']];
        endforeach;
	
        $event->gallery()->sync($medias);

        return response($event->load('institution', 'gallery', 'thumbnail'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\User
     */
    public function update(Request $request, $id)
    {
        $input = $request->input('event');
	$gallery = $request->input('gallery');
        $event = Event::find($id);
        $event->update($input);
	
	$medias = array();
        
        foreach($gallery as $index => $item):
	    $medias[$item['media_id']] = ['order' => $index, 'caption' => $item['caption']];
        endforeach;
        $event->gallery()->sync($medias);

        return response($event->load('institution', 'gallery', 'thumbnail'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Event::destroy($id), 200);
    }
}
