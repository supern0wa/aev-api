<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use App\Services\PaymentService;
use App\Models\Address;
use App\Models\Person;
use App\Models\Invoice;
use Illuminate\Http\Request;

class DonationController extends Controller
{
    protected $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PaymentService $service)
    {
        $this->service = $service;
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $donations = Donation::with('institution', 'invoice', 'invoice.person', 'invoice.transactions')
            ->paginate((int) $request->input('limit', 10))
            ->appends($request->all());
        
        return response($donations, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id)
    {
        return response(Donation::with('institution', 'invoice', 'invoice.person', 'invoice.person.address', 'invoice.transactions')
            ->find($id), 200);
    }
    
    /**
     *  Search Users
     * @param Request $request
     * @return type
     */
    
    public function search(Request $request)
    {
        $input = $request->all();

        try {
            $donations = Donation::with('institution', 'invoice', 'invoice.person', 'invoice.person.address', 'invoice.transactions')
                ->search($input['q'])
                ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            $e->getMessage();
            return response($e->getMessage(), 400);
        }

        return response($donations, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Institution
     */
    public function store(Request $request)
    {
        $this->validate($request, Donation::$validations);
        $input = $request->all();

        // Find person by email
        $person = Person::where('email', $input['person']['email'])->first();

        // Resolve person
        if (!$person) {
            $address = Address::create($input['address']);

            $input['person']['address_id'] = $address->id;
            $person = Person::create($input['person']);
        } else {
            $person->update($input['person']);
        }

        // Make an invoice
        $invoice = Invoice::create([
            'person_id' => $person->id
        ]);

        // Create donation
        $donation = $invoice->donations()
            ->create($input['donation']);

        // Proceed with checkout
        $redirect = $this->service
            ->checkout($donation->invoice);

        return response([
            'redirect_uri' => $redirect,
            'donation' => $donation
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Donation::destroy($id), 200);
    }
}
