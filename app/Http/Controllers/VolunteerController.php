<?php

namespace App\Http\Controllers;

use App\Models\Volunteer;
use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Person;



class VolunteerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        try {
            $volunteer = Volunteer::with('person','person.address')
                ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
            
        return response($volunteer, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id)
    {
        $volunteer = Volunteer::with('person','person.address')->find($id);
        return response($volunteer, 200);
    }
    
    /**
     *  Search Users
     * @param Request $request
     * @return type
     */
    
    public function search(Request $request)
    {
        $input = $request->all();
        try {
            $volunteers = Volunteer::with('person','person.address')
            ->search($input['q'])
            ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            $e->getMessage();
            return response($e->getMessage(), 400);
        }

        return response($volunteers, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Institution
     */
    public function store(Request $request)
    {
	$this->validate($request, Volunteer::$validations);
	$this->validate($request, Person::$validations);
	$this->validate($request, Address::$validations);

        $input = $request->all();

        try {
            // Find person by email
            $person = Person::where('email', $input['person']['email'])->first();

            // Resolve person
            if (!$person) {
                //Save Address
                $address = Address::create($input['address']);

                //Save Person
                $input['person']['address_id'] = $address->id;
                $person = Person::create($input['person']);
            } else {
                $person->update($input['person']);
                $person->address->update($input['address']);
            }

            // Save User
            $input['volunteer']['person_id'] = $person->id;
            $volunteer = Volunteer::create($input['volunteer']);
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return response($volunteer->load('person', 'person.address'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\User
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $volunteer = Volunteer::find($id);
        $person = Person::find($volunteer->person_id);
        $address = Address::find($person->address_id);
        
        $address->update($input['address']);
        $person->update($input['person']);
        $volunteer->update($input['volunteer']);

        return response($volunteer->load('person', 'person.address'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Volunteer::destroy($id), 200);
    }
}
