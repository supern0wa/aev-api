<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Person;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        try {
            $user = User::with('person', 'person.address')
                ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return response($user, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id)
    {
        $user = User::with('person', 'person.address', 'person.address.country')
            ->find($id);

        return response($user, 200);
    }
    
    /**
     *  Search Users
     * @param Request $request
     * @return type
     */
    public function search(Request $request)
    {
        $input = $request->all();

        try {
        $users = User::with('person', 'person.address', 'person.address.country')
            ->search($input['q'])
            ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            $e->getMessage();
            return response($e->getMessage(), 400);
        }

        return response($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\OAuth\User
     */
    public function store(Request $request)
    {
        $input = $request->all();

        try {
            //Save Address
            $address = Address::create($input['address']);

            //Save Person
            $input['person']['address_id'] = $address->id;
            $person = Person::create($input['person']);

            // Save User
            $input['user']['person_id'] = $person->id;
            $input['user']['password'] = md5($input['password']);
            $user = User::create($input['user']);
        } catch (\Exception $e) {
            return response(400, $e->getMessage());
        }

        return response($user->load('person'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\User
     */
    public function update(Request $request, $id)
    {
        $input_user = $request->input('user');
        $input_person = $request->input('person');
        $input_address = $request->input('address');
        $password = $request->input('password');
        $user = User::find($id);
        $person = Person::find($user->person_id);
        $address = Address::find($person->address_id);
        
        $address->update($input_address);
        $person->update($input_person);
        
        if (!empty($password))
            $user->password = md5($password);

        $user->username = $input_user['username'];
        $user->save();

        return response($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response($user, 200);
    }
}
