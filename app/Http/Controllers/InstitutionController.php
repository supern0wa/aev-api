<?php

namespace App\Http\Controllers;

use App\Models\Institution;
use Illuminate\Http\Request;
use App\Models\Address;

class InstitutionController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request) {
        $institution = Institution::with('address', 'thumbnail')
                ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());

        return response($institution, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id) {
        return response(Institution::with('address', 'address.country', 'thumbnail')->find($id), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Institution
     */
    public function store(Request $request) {
        $this->validate($request, Institution::$validations);
        $this->validate($request, Address::$validations);
        $input = $request->all();

        //Save Address
        $address = Address::create($input['address']);
        $input['institution']['address_id'] = $address->id;

        // Save Institution
        $institution = Institution::create($input['institution']);
        
        return response($institution->load('address', 'address.country', 'thumbnail'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\User
     */
    public function update(Request $request, $id) {
        $input = $request->all();
        $institution = Institution::find($id);
        $address = $institution->address;

        $address->update($input['address']);
        $institution->update($input['institution']);

        return response($institution->load('address','address.country', 'thumbnail'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id) {
        return response(Institution::destroy($id), 200);
    }

}
