<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;


class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        
        try {
            $pages = Page::with('thumbnail')
                ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
        return response($pages, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id)
    {
        $page = Page::with('thumbnail')->find($id);
        
        return response($page, 200);
    }
    
    /**
     *  Search Users
     * @param Request $request
     * @return type
     */
    public function search(Request $request)
    {
        $input = $request->all();
        try {
            $pages = Page::with('thumbnail')
            ->search($input['q'])
            ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            $e->getMessage();
            return response($e->getMessage(), 400);
        }

        return response($pages, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Institution
     */
    public function store(Request $request)
    {
	$this->validate($request, Page::$validations);
        $input = $request->input('page');
        $page = Page::create($input);

        return response($page->load('thumbnail'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\User
     */
    public function update(Request $request, $id)
    {
        $input = $request->input('page');
        $page = Page::find($id);
        $page->update($input);

        return response($page->load('thumbnail'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Page::destroy($id), 200);
    }
}
