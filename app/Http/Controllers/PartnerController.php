<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $partners = Partner::with('brand')
            ->paginate((int) $request->input('limit', 10))
            ->appends($request->all());

        return response($partners, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id)
    {
        $partner = Partner::with('brand')->find($id);
        
        return response($partner, 200);
    }
    
    /**
     *  Search Users
     * @param Request $request
     * @return type
     */
    
    public function search(Request $request)
    {
        $input = $request->all();
        $partners = Partner::with('brand')
            ->search($input['q'])
            ->paginate((int) $request->input('limit', 10))
            ->appends($request->all());

        return response($partners, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Institution
     */
    public function store(Request $request)
    {
        $input = $request->input('partner');
        $partner = Partner::create($input);

        return response($partner, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\User
     */
    public function update(Request $request, $id)
    {
        $input = $request->input('partner');
        $partner = Partner::find($id);
        $partner->update($input);

        return response($partner, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Partner::destroy($id), 200);
    }
}
