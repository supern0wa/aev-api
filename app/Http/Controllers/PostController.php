<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Category;


class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Paginate resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        try {
            $post = Post::with('categories', 'thumbnail', 'gallery')
                ->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }
        
        return response($post, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return [\AppModels\OAuth\User]
     */
    public function show($id)
    {
        $post = Post::with('categories', 'thumbnail', 'gallery')->find($id);
//	dd($post->gallery[0]->pivot->order);
	$post->gallery->map(function($gallery){
	    $gallery->caption = $gallery->pivot->caption;
	    $gallery->setOrderAttribute($gallery->pivot->order);
	});
        return response($post, 200);
    }
    
    /**
     *  Search Users
     * @param Request $request
     * @return type
     */
    
    public function search(Request $request)
    {
        $input = $request->all();

        try {
            $posts = Post::with('categories', 'thumbnail','gallery');

            // Keyword search
            if ($request->has('q'))
                $posts = $posts->search($input['q']);

            // Filter categories
            if ($request->has('cats'))
                $posts = $posts->whereHas('categories', function($query) use ($input) {
                    $query->whereIn('category_id', explode(',', $input['cats']));
                });

            // Paginate
            $posts = $posts->paginate((int) $request->input('limit', 10))
                ->appends($request->all());
        } catch (\Exception $e) {
            $e->getMessage();
            return response($e->getMessage(), 400);
        }

        return response($posts, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Models\Institution
     */
    public function store(Request $request)
    {
	
	$this->validate($request, Post::$validations);
        $input = $request->input('post');
        $gallery = $request->input('gallery');
        $categories = $request->input('categories');
        $post = Post::create($input);
        $medias = array();
        
        foreach($gallery as $index => $item):
          $medias[$item['media_id']] = ['order' => $index, 'caption' => $item['caption']];
        endforeach;
        $post->gallery()->sync($medias);
        $post->categories()->sync($categories);

        return response($post->load('categories', 'gallery', 'thumbnail'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \App\Models\OAuth\User
     */
    public function update(Request $request, $id)
    {
        $input = $request->input('post');
        $gallery = $request->input('gallery');
        $categories = $request->input('categories');
        $post = Post::find($id);
        $post->update($input);
        
        $medias = array();
        
        foreach($gallery as $index => $item):
          $medias[$item['media_id']] = ['order' => $index, 'caption' => $item['caption']];
        endforeach;
        $post->gallery()->sync($medias);
	
        $post->categories()->sync($categories);

        return response($post->load('categories', 'gallery', 'thumbnail'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return boolean
     */
    public function destroy($id)
    {
        return response(Post::destroy($id), 200);
    }
}
