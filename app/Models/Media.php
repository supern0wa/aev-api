<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model {

    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'medias';
    protected $order;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'caption',
        'file_name',
        'disk',
        'size',
        'data',
    ];
    
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'name',
        'file_name',
        'caption',
        'data',
        'disk',
        'size',
        'src',
	'order',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'src',
	'order'
    ];

    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];
    
    /**
     * Get the file SRC.
     *
     * @param  string  $value
     * @return string
     */
    public function getSrcAttribute()
    {
        return env('STORAGE_URL') . '/' . $this->disk . '/' . $this->file_name;
    }
    
    public function getOrderAttribute()
    {
        return $this->order;
    }
    
    public function setOrderAttribute($order){
	$this->order = $order;
    }
    
    /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        return $query->where('name', "LIKE", "%$q%")
            ->orWhere("caption", "LIKE", "%$q%");
    }

}

