<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'content',
        'date',
        'institution_id',
        'thumbnail_id'
    ];
    
    static $validations = [
	    'event.title' => 'required',
	    'event.slug' => 'required',
	    'event.excerpt' => 'required',
	    'event.date' => 'required|date_format:Y-m-d',
	    'event.content' => 'required',
	    'event.institution_id' => 'required',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
    ];

    /**
     * Get the category record associated with the post.
     */
    public function institution()
    {
        return $this->belongsTo('App\Models\Institution');
    }
    
    public function thumbnail()
    {
        return $this->belongsTo('App\Models\Media');
    }
    
    public function gallery()
    {
        return $this->belongsToMany('App\Models\Media')->withPivot('caption', 'order');
    }
    
    /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        return $query->whereHas('institution', function($query) use($q){
            $query->where("name", "LIKE", "%$q%");
        })
        ->orWhere('title', "LIKE", "%$q%")
        ->orWhere("slug", "LIKE", "%$q%")
        ->orWhere('excerpt', 'LIKE', "%$q%");
    }
    

}
