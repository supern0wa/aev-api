<?php

namespace App\Models;

interface IPurchasable {
    public function getDescription();
    public function getPrice();
}
