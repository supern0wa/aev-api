<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'brand_id'
    ];
    
    /**
     * The attributes that are visible.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'name',
        'brand_id',
        'brand'
    ];
    
    /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        return $query->where('name', "LIKE", "%$q%");
    }
    
    /**
     * Get the Media record associated with the Partner.
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Media');
    }
    

}
