<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'excerpt',
        'content',
        'thumbnail_id'
    ];
    
    static $validations = [
	    'page.title' => 'required',
	    'page.slug' => 'required',
	    'page.excerpt' => 'required',
	    'page.content' => 'required',
	    'page.thumbnail_id' => 'required',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
    ];
    
    public function thumbnail()
    {
        return $this->belongsTo('App\Models\Media');
    }
    
    /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        return $query->where('title', "LIKE", "%$q%")
            ->orWhere("slug", "LIKE", "%$q%")
            ->orWhere('excerpt', 'LIKE', "%$q%");
    }
}
