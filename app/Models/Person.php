<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Person extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'cpf',
        'rg',
        'phone',
        'mobile',
        'address_id',
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'first_name',
        'last_name',
        'full_name',
        'email',
        'cpf',
        'rg',
        'phone',
        'mobile',
        'address',
        'address_id',
        'created_at',
        'updated_at'
    ];
    
    static $validations = [
	    'person.first_name' => 'required',
	    'person.last_name' => 'required',
	    'person.email' => 'required',
	    'person.cpf' => 'required|max:14',
	    'person.rg' => 'required|max:10',
	    'person.mobile' => 'required',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name'
    ];

    /**
     * Get the user record associated with the person.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    /**
     * Get the address record associated with the person.
     */
    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }
    
    /**
     * Get the user record associated with the person.
     */
    public function volunteer()
    {
        return $this->hasOne('App\Models\Volunteer');
    }

    /**
     * Get the user's first name.
     *
     * @param  string  $value
     * @return string
     */
    public function getFullNameAttribute($value)
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }
}
