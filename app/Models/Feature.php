<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feature extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'excerpt',
        'link',
        'order',
        'thumbnail_id'
    ];
    
    static $validations = [
	    'feature.title' => 'required',
	    'feature.excerpt' => 'required',
	    'feature.link' => 'required',
	    'feature.order' => 'required',
	    'feature.thumbnail_id' => 'required',
    ];

    /**
     * Get the category record associated with the feature.
     */
    
    public function thumbnail()
    {
        return $this->belongsTo('App\Models\Media');
    }
    
    /**
     * Search Method
     * @param type $q
     * @return type
     */
    public function scopeSearch($query, $q)
    {
        return $query->where('title', "LIKE", "%$q%")
            ->orWhere('excerpt', 'LIKE', "%$q%");
    }
}
