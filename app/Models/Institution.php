<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Institution extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'content',
        'location',
        'address_id',
        'thumbnail_id'
    ];
    
    static $validations = [
        'institution.name' => 'required',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'location' => 'array',
        'content' => 'array'
    ];

    /**
     * Get the person record associated with the user.
     */
    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }

    /**
     * Get the person record associated with the user.
     */
    public function thumbnail()
    {
        return $this->belongsTo('App\Models\Media');
    }
}
