<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug'
    ];
    
    static $validations = [
	    'category.name' => 'required',
	    'category.slug' => 'required',
    ];

    /**
     * Get the posts records associated with the category.
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }
}
