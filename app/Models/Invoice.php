<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Notifications\InvoicePending;

class Invoice extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'discount',
        'person_id'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'total',
        'discount',
        'person',
        'person_id',
        'transactions'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'total'
    ];

    public function person()
    {
        return $this->belongsTo('App\Models\Person');
    }

    public function donations()
    {
        return $this->hasMany('App\Models\Donation');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function updateTotal($value)
    {
        $this->attributes['total'] += $value;
    }

    // Return this->donations && this->products
    public function getItems()
    {
        return $this->donations;
    }

    public function getTotalAttribute()
    {
        $total = 0;

        foreach ($this->getItems() as $item) {
            $total += $item->getPrice();
        }

        return $total;
    }

    /**
     * Send pending notification to donator.
     *
     * @return void
     */
    public function sendPendingNotification()
    {
        return $this->notify(new InvoicePending($this));
    }

    /**
     * Route notifications for the mail channel.
     *
     * @return string
     */
    public function routeNotificationForMail()
    {
        return $this->person->email;
    }
}
