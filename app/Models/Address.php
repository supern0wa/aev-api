<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street',
        'state',
        'number',
        'complement',
        'city',
        'zipcode',
        'neighbourhood',
        'country_id'
    ];

    static $validations = [
	    'address.street' => 'required',
	    'address.neighbourhood' => 'required',
	    'address.city' => 'required',
	    'address.state' => 'required',
	    'address.zipcode' => 'required',
	    'address.number' => 'required',
	    'address.country_id' => 'required',
    ];
    
    /**
     * Get the country record associated with the address.
     */
    public function country()
    {
        return $this->belongsTo('Webpatser\Countries\Countries');
    }
}
