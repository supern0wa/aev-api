<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'invoice_id'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'status',
        'invoice',
        'invoice_id',
        'created_at',
        'updated_at'
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }
}
