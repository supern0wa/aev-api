<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class InvoicePending extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $invoice;

    /**
     * Create a notification instance.
     *
     * @param  App\Models\Invoice  $invoice
     * @return void
     */
    public function __construct($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Nova Doação')
            ->line('Este é um email de confirmação.')
            ->line('Sua transação em http://ongaev.com.br está quase concluida!')
            ->action('Confirmar Pagamento', 'http://ongaev.com.br')
            ->line('Qualquer problema ou dúvida, entre em contato conosco através do email: contato@ongaev.com.br');
    }
}
