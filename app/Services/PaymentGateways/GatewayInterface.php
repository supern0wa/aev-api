<?php

namespace App\Services\PaymentGateways;

use Illuminate\Http\Request;

interface GatewayInterface
{
    public function authorize();
    public function purchase($invoice);
    public function getTransaction($code);
    public function getTransactionStatus($code);
    public function resolveStatus($value);
    public function handleCallback(Request $request);
}
