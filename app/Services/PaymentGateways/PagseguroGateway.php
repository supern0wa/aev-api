<?php

namespace App\Services\PaymentGateways;

use PagSeguro\Library;
use PagSeguro\Configuration\Configure;
use PagSeguro\Services\Session;
use PagSeguro\Services\Application;
use PagSeguro\Domains\Requests\Payment;
use PagSeguro\Enum\Shipping;
use PagSeguro\Enum\PaymentMethod;
use PagSeguro\Services\Transactions;
use Illuminate\Http\Request;

class PagseguroGateway implements GatewayInterface
{
    /**
     * Create a new gateway instance.
     *
     * @return void
     */
    public function __construct()
    {
        $config = config('payment.gateways.pagseguro');

        // Check if library was already initialized
        if (!defined('PS_BASEPATH'))
            Library::initialize();

        // Set environment to sandbox or production
        Configure::setEnvironment($config['environment']);

        // If seller profile flow
        Configure::setAccountCredentials(
            $config['email'], $config['token']
        );
    }

    /**
     * Retrieves access token.
     *
     * @return string $access_token
     */
    public function authorize()
    {
        // Get seller credentials
        $credentials = Configure::getAccountCredentials();

        // Generate session code
        $session = Session::create($credentials);

        // Return session code
        return $session->getResult();
    }

    /**
     * Purchase items from a invoice.
     *
     * @param  string  $code
     * @return \PagSeguro\Parsers\Transaction\Response
     */
    public function purchase($invoice)
    {
        $payment = new Payment();
        $person = $invoice->person;

        // Convert invoice items to PagSeguro
        foreach ($invoice->getItems() as $item) {
            $payment->addItems()->withParameters(
                $item->id,
                $item->getDescription(),
                1,
                $item->getPrice()
            );
        }

        // Set transaction currency
        $payment->setCurrency("BRL");
        $payment->setReference($invoice->id);

        // Set customer name
        $payment->setSender()
            ->setName($person->first_name . " " . $person->last_name);

        // Set customer email
        $payment->setSender()
            ->setEmail($person->email);

        // Set customer phone
        $payment->setSender()
            ->setPhone($person->phone);

        // TODO: Set customer documents
        $payment->setSender()
            ->setDocument()
            ->withParameters(
                'CPF',
                '01358660492'
            );

        // TODO: Set customer address
        $payment->setShipping()
            ->setAddress()
            ->withParameters(
                $person->address->street,
                $person->address->number,
                $person->address->neighbourhood,
                $person->address->zipcode,
                $person->address->city,
                $person->address->state,
                $person->address->country->currency_code,
                $person->address->complement
            );

        // Set shipping method
        $payment->setShipping()
            ->setType()
            ->withParameters(Shipping\Type::NOT_SPECIFIED);

         // Add a group and/or payment methods name
         $payment->acceptPaymentMethod()->groups(
             PaymentMethod\Group::CREDIT_CARD,
             PaymentMethod\Group::BALANCE,
             PaymentMethod\Group::BOLETO,
             PaymentMethod\Group::DEPOSIT
         );

        // Itau
        $payment->acceptPaymentMethod()
            ->name(PaymentMethod\Name::DEBITO_ITAU);

        // Bradesco
        $payment->acceptPaymentMethod()
            ->name(PaymentMethod\Name::DEBITO_BRADESCO);

        // Banco do Brasil
        $payment->acceptPaymentMethod()
            ->name(PaymentMethod\Name::DEBITO_BANCO_BRASIL);

        // Retrieve payment URL
        $credentials = Configure::getAccountCredentials();

        // Try to submit
        try {
            $redirect = $payment->register($credentials);
        } catch (\Exception $e) {
            throw $e;
        }

        return $redirect;
    }

    /**
     * Get transaction data by code.
     *
     * @param  string  $code
     * @return \PagSeguro\Parsers\Transaction\Response
     */
    public function getTransaction($code)
    {
        return Transactions\Search\Code::search(
            Configure::getAccountCredentials(), $code
        );
    }

    /**
     * Get converted transaction status.
     *
     * @param  string  $code
     * @return string  pending | accepted | rejected | null
     */
    public function getTransactionStatus($code)
    {
        $transaction = $this->getTransaction($code);

        return $this->resolveStatus($transaction->getStatus());
    }

    /**
     * Get notification data by code.
     *
     * @param  string  $code
     * @return \PagSeguro\Parsers\Transaction\Response
     */
    public function getNotification()
    {
        return \PagSeguro\Services\Transactions\Notification::check(
            Configure::getAccountCredentials()
        );
    }

    public function resolveStatus($value) {
        // Translate PagSeguro status codes
        switch ($value) {
            case 1:
            case 2:
            case 5:
                return 'pending';
                break;
            case 3:
            case 4:
                return 'accepted';
                break;
            case 6;
            case 7:
                return 'canceled';
                break;
            default:
                return null;
                break;
        }
    }

    public function handleCallback(Request $request)
    {
        // Get transaction by code
        $transaction = $this->getNotification();

        return [
            'status' => $this->resolveStatus($transaction->getStatus()),
            'reference' => $transaction->getReference()
        ];
    }
}
