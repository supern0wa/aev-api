<?php

namespace App\Services;

use App\Services\PaymentGateways\GatewayInterface;
use App\Models\Person;
use App\Models\Invoice;
use App\Models\Transaction;
use Illuminate\Http\Request;

class PaymentService
{
    /**
     * The payment gateway gateway
     */
    protected $gateway;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct(GatewayInterface $gateway = null)
    {
        $this->gateway = $gateway;
    }

    /**
     * Return the service's current payment gateway.
     *
     * @return \App\Services\PaymentGateways\GatewayInterface
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * Replace current gateway with a new one.
     *
     * @param \App\Services\PaymentGateways\GatewayInterface
     * @return void
     */
    public function setGateway(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * Authorizes and submit checkout.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return Response
     */
    public function checkout($invoice)
    {
        // Process and get redirect uri
        $redirect = $this->gateway->purchase($invoice);

        // Notifies customer
        $invoice->sendPendingNotification();

        return $redirect;
    }

    /**
     * Notification callback updates the transaction callback data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function callback(Request $request)
    {
        // Resolve gateway notification data
        $data = $this->gateway->handleCallback($request);

        try {
            // Find invoice with reference code
            $invoice = Invoice::find($data['reference']);

            // Create transaction
            $transaction = Transaction::create([
                'invoice_id' => $invoice->id,
                'status' => $data['status']
            ]);
        } catch (\Exception $e) {
            throw $e;
        }

        // Return transaction object
        return $transaction;
    }
}
