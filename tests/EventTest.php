<?php

use App\Models\Event;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class EventTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Event show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $event = factory('App\Models\Event')->create();

        $this->get('/events/' . $event->id, $this->getHeaders())
            ->seeJsonStructure([
                'title', 'slug', 'excerpt', 'content', 'date',
                'institution','thumbnail_id', 'gallery',
                'thumbnail' => [
                    'id', 'name', 'file_name', 'size', 'data'
                ]
            ]);
    }

    /**
     * Event create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $event = factory('App\Models\Event')->raw();
	$medias = factory('App\Models\Media', 'image', 2)->create();

        $response = $this->post('/events', [
            'event' => $event,
	    'gallery' => [
                [
                  'media_id' => $medias[0]->id,
                  'caption' => 'Teste de Caption'
                ],
                [
                    'media_id' => $medias[1]->id,
                    'caption' => 'Teste de Caption 2'
                ]
	    ]
        ], $this->getHeaders())
        ->seeJson([
            'title' => $event['title'],
            'date' => $event['date']
        ])
        ->seeJsonStructure([
            'institution', 'thumbnail', 'gallery'
        ]);
	
	$content = json_decode($response->response->getContent(), true);
        
        $this->assertEquals($medias[0]->id, $content['gallery'][0]['id']);
        $this->assertEquals($medias[1]->id, $content['gallery'][1]['id']);
    }

    /**
     * Event update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $event = factory('App\Models\Event')->create();
	$medias = factory('App\Models\Media', 'image', 4)->create();
        $media_ids = [];
	
	foreach($medias as $index => $item):
          $media_ids[$item['media_id']] = ['order' => $index, 'caption' => $item['caption']];
        endforeach;
	
	$event->gallery()->sync($media_ids);

        $response = $this->put('/events/' . $event->id, [
            'event' => [
                'title' => 'Ragnarok',
                'slug' => 'ragnarok',
                'date' => '1990-12-05'
            ],
            'gallery' => [
                [
                    'media_id' => $medias[0]->id,
                    'caption' => 'Teste de Caption',
                ],
                [
                    'media_id' => $medias[1]->id,
                    'caption' => 'Teste de Caption'
                ],
                [
                    'media_id' => $medias[2]->id,
                    'caption' => 'Teste de Caption'
                ],
            ]
        ], $this->getHeaders())
        ->seeJson([
            'title' => 'Ragnarok',
            'slug' => 'ragnarok',
            'date' => '1990-12-05'
        ])
        ->seeJsonStructure([
            'institution', 'thumbnail', 'gallery'
        ]);
	
	$content = json_decode($response->response->getContent(), true);
        
        $this->assertEquals(3, count($content['gallery']));
    }

    /**
     * Paginate event route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\Event', 40)->create();

        $this->get('/events?limit=5', $this->getHeaders())
            ->seeJson([
                'total' => 40,
                'per_page' => 5,
                'last_page' => 8
            ])
            ->seeJsonStructure([
                'data' => [
                    ['institution', 'thumbnail', 'gallery']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search event route test.
     *
     * @return void
     */
    public function testSearch()
    {
        factory('App\Models\Event', 40)->create();
        factory('App\Models\Event')->create([
            'title' => 'Asgard rules!'
        ]);

        $this->get('/events/search?q=asgard&limit=1', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['institution', 'thumbnail', 'gallery']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete event route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $event = factory('App\Models\Event')->create();

        $this->delete('/events/' . $event->id, [], $this->getHeaders());

        $this->notSeeInDatabase('events', [
            'id' => $event->id,
            'deleted_at' => null
        ]);
    }
}
