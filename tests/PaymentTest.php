<?php

use Illuminate\Support\Facades\Notification;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Services\PaymentGateways\PagseguroGateway;
use App\Services\PaymentService;

class PaymentTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->service = new PaymentService(new PagseguroGateway());
    }

    public function testCheckout()
    {
        $invoice = factory('App\Models\Invoice')->create();
        $donation = factory('App\Models\Donation')->create([
            'invoice_id' => $invoice->id
        ]);

        // The total should be the same as the donation value
        $this->assertEquals($invoice->total, $donation->value);

        // Proceed with checkout
        $result = $this->service->checkout($invoice);

        // Check if notification was sent
        Notification::assertSentTo(
            $invoice,
            \App\Notifications\InvoicePending::class,
            function ($notification, $channels) use ($invoice) {
                return $notification->invoice->id === $invoice->id;
            }
        );
    }

    public function testCallback()
    {
        $invoice = factory('App\Models\Invoice')->create();
        $donation = factory('App\Models\Donation')->create([
            'invoice_id' => $invoice->id
        ]);

        // Wrap PagseguroGateway class
        $gateway = $this->getMockBuilder('\App\Services\PaymentGateways\PagseguroGateway')
            ->setMethods(['handleCallback'])
            ->getMock();

        // Rewrite getTransaction method
        $gateway->method('handleCallback')
            ->willReturn([
                'status' => 'accepted',
                'reference' => $invoice->id
            ]);

        $service = new PaymentService($gateway);
        $response = $service->callback(new \Illuminate\Http\Request());

        $this->assertInstanceOf(\App\Models\Transaction::class, $response);
    }
}
