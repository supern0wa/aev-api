
<?php

use App\Models\Volunteer;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class VolunteerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Volunteer show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $volunteer = factory('App\Models\Volunteer')->create();

        $this->get('/volunteers/' . $volunteer->id, $this->getHeaders())
            ->seeJsonStructure([
                'civil_status', 'birthday', 'ocupation',
                'religion', 'reason', 'suggestions', 'abilities', 'days',
                'shifts', 'person_id', 'person'
            ]);
    }

    /**
     * Volunteer create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $volunteer = factory('App\Models\Volunteer')->raw(['person_id' => null]);
        $person = factory('App\Models\Person')->raw(['address_id' => null]);
        $address = factory('App\Models\Address')->raw();

        $this->post('/volunteers', [
            'volunteer' => $volunteer,
            'person' => $person,
            'address' => $address
        ], $this->getHeaders())
        ->seeJson([
            'civil_status' => $volunteer['civil_status'],
            'birthday' => $volunteer['birthday'],
            'ocupation' => $volunteer['ocupation'],
            'religion' => $volunteer['religion'],
            'reason' => $volunteer['reason'],
            'suggestions' => $volunteer['suggestions'],
            'abilities' => $volunteer['abilities'],
            'shifts' => $volunteer['shifts'],
        ])
        ->seeJsonStructure([
            'person' => [
                'address'
            ]
        ]);
    }

    /**
     * Volunteer update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $volunteer = factory('App\Models\Volunteer')->create();

        $this->put('/volunteers/' . $volunteer->id, [
            'volunteer' => [
                'civil_status' => 'married',
                'birthday' => '1990-12-05',
                'ocupation' => 'Developer',
                'religion' => 'Wut',
                'reason' => 'Because god wants so.',
                'suggestions' => 'Hire me, of course.',
                'abilities' => 'Cook, clean, play, draw, etc.',
                'days' => [
                    'sun' => false,
                    'mon' => true,
                    'thu' => true,
                    'wed' => true,
                    'tue' => true,
                    'fri' => true,
                    'sat' => false
                ],
                'shifts' => [
                    'first' => false,
                    'second' => true
                ]
            ],
            'person' => [
                'first_name' => 'Giseudo',
                'last_name' => 'Oliveira'
            ],
            'address' => [
                'street' => 'Rua Sao Miguel',
                'neighbourhood' => 'Varadouro',
                'number' => '220',
                'zipcode' => '58010-270',
                
            ]
        ])->seeJsonStructure([
            'civil_status', 'birthday', 'ocupation', 'religion',
            'reason', 'suggestions', 'abilities', 'days', 'shifts',
            'person' => [
                'address'
            ]
        ]);

        $this->seeInDatabase('volunteers', [
            'civil_status' => 'married',
            'birthday' => '1990-12-05',
            'ocupation' => 'Developer',
            'religion' => 'Wut',
            'reason' => 'Because god wants so.',
            'suggestions' => 'Hire me, of course.',
            'abilities' => 'Cook, clean, play, draw, etc.'
        ]);

        $this->seeInDatabase('people', [
            'first_name' => 'Giseudo',
            'last_name' => 'Oliveira'
        ]);

        $this->seeInDatabase('addresses', [
            'street' => 'Rua Sao Miguel',
            'number' => '220',
            'neighbourhood' => 'Varadouro'
        ]);
    }

    /**
     * Paginate volunteer route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\Volunteer', 40)->create();

        $this->get('/volunteers?limit=5', $this->getHeaders())
            ->seeJson([
                'per_page' => 5,
                'last_page' => 8,
                'total' => 40
            ])
            ->seeJsonStructure([
                'data' => [
                    ['person']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search volunteer route test.
     *
     * @return void
     */
    public function testSearch()
    {
        factory('App\Models\Volunteer', 40)->create();
        factory('App\Models\Volunteer')->create([
            'person_id' => factory('App\Models\Person')->create([
                'cpf' => '000.000.000-00'
            ])->id
        ]);

        $this->get('/volunteers/search?q=000.000.000-00&limit=1', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['person']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete volunteer route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $volunteer = factory('App\Models\Volunteer')->create();

        $this->delete('/volunteers/' . $volunteer->id, [], $this->getHeaders());

        $this->notSeeInDatabase('volunteers', [
            'id' => $volunteer->id,
            'deleted_at' => null
        ]);
    }
}
