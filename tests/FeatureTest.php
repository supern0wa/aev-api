<?php

use App\Models\Feature;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class FeatureTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Post show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $feature = factory('App\Models\Feature')->create();

        $this->get('/features/' . $feature->id, $this->getheaders())
            ->seeJsonStructure([
                'title', 'excerpt', 'link', 'order', 'thumbnail'
            ]);
    }

    /**
     * Post create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $feature = factory('App\Models\Feature')->raw();

        $response = $this->post('/features', [
            'feature' => $feature,
        ], $this->getheaders())
        ->seeJson([
            'title' => $feature['title'],
            'excerpt' => $feature['excerpt'],
            'link' => $feature['link'],
            'order' => $feature['order'],
        ])
        ->seeJsonStructure([
            'thumbnail'
        ]);
        
    }

    /**
     * Post update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $feature = factory('App\Models\Feature')->create();
	
        $response = $this->put('/features/' . $feature->id, [
            'feature' => [
                'title' => 'Hello World',
                'excerpt' => 'Sit vitae voluptas sint non voluptates.',
                'link' => 'http://google.com',  
                'order' => 1,  
            ]
        ], $this->getheaders())
        ->seeJsonStructure([
            'title', 'thumbnail','excerpt','link','order'
        ]);
    }

    /**
     * Paginate post route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\Feature', 40)->create();

        $this->get('/features?limit=5', $this->getheaders())
            ->seeJson([
                'total' => 40,
                'per_page' => 5,
                'last_page' => 8
            ])
            ->seeJsonStructure([
                'data' => [
                    ['thumbnail']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search post route test.
     *
     * @return void
     */
    public function testSearch()
    {
        factory('App\Models\Feature', 40)->create();
        factory('App\Models\Feature')->create([
            'title' => 'Supernova rocks!',
        ]);

        $this->get('/features/search?q=supernova&limit=1', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['thumbnail']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete post route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $feature = factory('App\Models\Feature')->create();

        $this->delete('/features/' . $feature->id, [], $this->getheaders());

        $this->notSeeInDatabase('features', [
            'id' => $feature->id,
            'deleted_at' => null
        ]);
    }
}
