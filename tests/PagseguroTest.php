<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Services\PaymentGateways\PagseguroGateway;
use PagSeguro\Services\Transactions;

class PagseguroTest extends TestCase
{
    private $gateway;

    public function setUp()
    {
        parent::setUp();

        $this->gateway = new PagseguroGateway();
    }

    public function testAuthorize()
    {
        $token = $this->gateway->authorize();

        // Validate token string length
        $this->assertEquals(strlen($token) == 32, true);
    }

    public function testPurchase()
    {
        $donation = factory('App\Models\Donation')->create();

        // Get result
        $redirect_uri = $this->gateway
            ->purchase($donation->invoice);

        // Verify if redirect url is valid
        $this->assertEquals(filter_var(
            $redirect_uri, FILTER_VALIDATE_URL
        ), $redirect_uri);
    }

    public function testStatus()
    {
        // Wrap PagseguroGateway class
        $gateway = $this->getMockBuilder('\App\Services\PaymentGateways\PagseguroGateway')
            ->setMethods(['getTransaction'])
            ->getMock();

        // Create a new response object
        $response = new \PagSeguro\Parsers\Transaction\Response();

        // Rewrite transaction method
        $gateway->method('getTransaction')
            ->willReturn($response);

        // Check status from 1 to 7
        for ($i = 1; $i <= 1; $i++) {
            $response->setStatus($i);
            $status = $gateway->getTransactionStatus('validtransactioncode');

            // Pending status
            if ($i == 1 || $i == 2 || $i == 5)
                $this->assertEquals($status, 'pending');

            // Accepted status
            if ($i == 3 || $i == 4)
                $this->assertEquals($status, 'accepted');

            // Canceled status
            if ($i == 6 || $i == 7)
                $this->assertEquals($status, 'canceled');
        }
    }

    public function testCallback()
    {
        // Wrap PagseguroGateway class
        $gateway = $this->getMockBuilder('\App\Services\PaymentGateways\PagseguroGateway')
            ->setMethods(['getNotification'])
            ->getMock();

        // Create a new response object
        $transaction = new \PagSeguro\Parsers\Transaction\Response();

        // Set response status to accepted
        $transaction->setStatus(3);
        $transaction->setReference(2);

        // Rewrite getTransaction method
        $gateway->method('getNotification')
            ->willReturn($transaction);

        $request = new \Illuminate\Http\Request();
        $request->replace([
            'notificationCode' => 'validnotificationcode',
            'notificationType' => 'transaction'
        ]);

        $data = $gateway->handleCallback($request);

        $this->assertArraySubset($data, [
            'status' => 'accepted',
            'reference' => 2
        ]);
    }
}
