<?php

use App\Models\Page;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PageTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Page show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $page = factory('App\Models\Page')->create();

        $this->get('/pages/' . $page->id, $this->getHeaders())
            ->seeJsonStructure([
                'title', 'slug', 'excerpt', 'content', 'thumbnail_id',
                'thumbnail' => [
                    'id', 'name', 'file_name', 'size', 'data'
                ]
            ]);
    }

    /**
     * User create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $page = factory('App\Models\Page')->raw();

        $this->post('/pages', [
            'page' => $page
        ], $this->getHeaders())
        ->seeJson([
            'title' => $page['title']
        ])
        ->seeJsonStructure([
            'thumbnail'
        ]);
    }

    /**
     * User update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $page = factory('App\Models\Page')->create();

        $this->put('/pages/' . $page->id, [
            'page' => [
                'title' => 'Hello World',
                'slug' => 'hello-world',
                'excerpt' => 'Sit vitae voluptas sint non voluptates.',
                'content' => 'Fuga totam reiciendis qui architecto fugiat nemo. Consequatur recusandae qui cupiditate eos quod.',
            ]
        ], $this->getHeaders())
        ->seeJson([
            'title' => 'Hello World'
        ])
        ->seeJsonStructure([
            'thumbnail'
        ]);
    }

    /**
     * Paginate page route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\Page', 40)->create();

        $this->get('/pages?limit=5', $this->getHeaders())
            ->seeJson([
                'total' => 40,
                'per_page' => 5,
                'last_page' => 8
            ])
            ->seeJsonStructure([
                'data' => [
                    ['thumbnail']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search user route test.
     *
     * @return void
     */
    public function testSearch()
    {
        factory('App\Models\Page', 40)->create();
        factory('App\Models\Page')->create([
            'title' => 'Supernova rocks!',
        ]);

        $this->get('/pages/search?q=supernova&limit=1', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['thumbnail']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete page route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $page = factory('App\Models\Page')->create();

        $this->delete('/pages/' . $page->id, [], $this->getHeaders());

        $this->notSeeInDatabase('pages', [
            'id' => $page->id,
            'deleted_at' => null
        ]);
    }
}
