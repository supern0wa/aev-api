<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Services\PaymentGateways\CieloGateway;

class CieloTest extends TestCase
{
    private $gateway;

    public function setUp()
    {
        parent::setUp();

        $this->gateway = new CieloGateway();
    }

    public function testAuthorize()
    {
    }

    public function testPurchase()
    {
        $donation = factory('App\Models\Donation')->create();

        // Get result
        $result = $this->gateway
            ->purchase($donation->invoice);

        // Verify returned array
        $this->assertArrayHasKey('redirect_uri', $result);

        // Verify if redirect url is valid
        $this->assertEquals(filter_var(
            $result['redirect_uri'], FILTER_VALIDATE_URL
        ), $result['redirect_uri']);
    }

    public function testStatus()
    {
    }

    public function testHandle()
    {
        // test callback
    }
}
