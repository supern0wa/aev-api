<?php

use App\Models\Category;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Category show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $category = factory('App\Models\Category')->create();

        $this->get('/categories/' . $category->id, $this->getheaders())
            ->seeJsonStructure([
                'name', 'slug'
            ]);
    }

    /**
     * User create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $category = factory('App\Models\Category')->raw();

        $this->post('/categories', [
            'category' => $category
        ], $this->getheaders())
        ->seeJson([
            'name' => $category['name'],
            'slug' => $category['slug']
        ]);
    }

    /**
     * User update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $category = factory('App\Models\Category')->create();

        $this->put('/categories/' . $category->id, [
            'category' => [
                'name' => 'Hello World',
                'slug' => 'hello-world'
            ]
        ], $this->getheaders())
        ->seeJson([
            'name' => 'Hello World'
        ]);
    }

    /**
     * Delete user route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $category = factory('App\Models\Category')->create();

        $this->delete('/categories/' . $category->id, [], $this->getheaders());

        $this->notSeeInDatabase('categories', [
            'id' => $category->id,
            'deleted_at' => null
        ]);
    }
}
