<?php

use App\Models\Post;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class PostTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Post show route test.
     *
     * @return void
     */
    public function testShow()
    {
        $post = factory('App\Models\Post')->create();

        $this->get('/posts/' . $post->id, $this->getheaders())
            ->seeJsonStructure([
                'title', 'slug', 'excerpt', 'content',
                'categories', 'thumbnail', 'gallery'
            ]);
    }

    /**
     * Post create route test.
     *
     * @return void
     */
    public function testCreate()
    {
        $post = factory('App\Models\Post')->raw();
        $medias = factory('App\Models\Media', 'image', 2)->create();
        $categories = factory('App\Models\Category', 2)->create();

        $response = $this->post('/posts', [
            'post' => $post,
            'gallery' => [
                [
                  'media_id' => $medias[0]->id,
                  'caption' => 'Teste de Caption'
                ],
                [
                    'media_id' => $medias[1]->id,
                    'caption' => 'Teste de Caption 2'
                ]
            ],
	    'categories' => [ $categories[0]->id, $categories[1]->id ]
        ], $this->getheaders())
        ->seeJson([
            'title' => $post['title'],
            'slug' => $post['slug']
        ])
        ->seeJsonStructure([
            'categories', 'thumbnail', 'gallery'
        ]);
        
        $content = json_decode($response->response->getContent(), true);
        
        $this->assertEquals($medias[0]->id, $content['gallery'][0]['id']);
        $this->assertEquals($medias[1]->id, $content['gallery'][1]['id']);
        $this->assertEquals($categories[0]->id, $content['categories'][0]['id']);
        $this->assertEquals($categories[1]->id, $content['categories'][1]['id']);
        
    }

    /**
     * Post update route test.
     *
     * @return void
     */
    public function testUpdate()
    {
        $post = factory('App\Models\Post')->create();
        $medias = factory('App\Models\Media', 'image', 4)->create();
        $categories = factory('App\Models\Category', 4)->create();
        
	$media_ids = [];
        $categories_ids = [];
        
        foreach($medias as $index => $item):
          $media_ids[$item['media_id']] = ['order' => $index, 'caption' => $item['caption']];
        endforeach;
	
        foreach($categories as $item):
          $categories_ids[] = $item['id'];
        endforeach;
        
        
        $post->gallery()->sync($media_ids);
        $post->categories()->sync($categories_ids);

        $response = $this->put('/posts/' . $post->id, [
            'post' => [
                'title' => 'Hello World',
                'slug' => 'hello-world',
                'excerpt' => 'Sit vitae voluptas sint non voluptates.',
                'content' => 'Fuga totam reiciendis qui architecto fugiat nemo. Consequatur recusandae qui cupiditate eos quod.',  
            ],
            'gallery' => [
                [
                    'media_id' => $medias[0]->id,
                    'caption' => 'Teste de Caption',
                ],
                [
                    'media_id' => $medias[1]->id,
                    'caption' => 'Teste de Caption'
                ],
                [
                    'media_id' => $medias[2]->id,
                    'caption' => 'Teste de Caption'
                ],
            ],
	    'categories' => [ $categories[0]->id, $categories[1]->id,  $categories[2]->id]
        ], $this->getheaders())
        ->seeJson([
            'title' => 'Hello World',
            'slug' => 'hello-world',
        ])
        ->seeJsonStructure([
            'categories', 'thumbnail', 'gallery'
        ]);
        
        $content = json_decode($response->response->getContent(), true);
        
        $this->assertEquals(3, count($content['gallery']));
        $this->assertEquals(3, count($content['categories']));
    }

    /**
     * Paginate post route test.
     *
     * @return void
     */
    public function testPaginate()
    {
        factory('App\Models\Post', 40)->create();

        $this->get('/posts?limit=5', $this->getheaders())
            ->seeJson([
                'total' => 40,
                'per_page' => 5,
                'last_page' => 8
            ])
            ->seeJsonStructure([
                'data' => [
                    ['categories', 'thumbnail', 'gallery']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Search post route test.
     *
     * @return void
     */
    public function testSearch()
    {
        // Test keyword search
        factory('App\Models\Post', 40)->create();
        factory('App\Models\Post')->create([
            'title' => 'Supernova rocks!',
        ]);

        $this->get('/posts/search?q=supernova&limit=1', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 1
            ])
            ->seeJsonStructure([
                'data' => [
                    ['thumbnail']
                ], 'next_page_url', 'prev_page_url'
            ]);

        // Test categories filter
        $categories = factory('App\Models\Category', 2)->create();

        factory('App\Models\Post', 5)->create()->each(function($post) use ($categories) {
            $post->categories()->attach($categories[0]->id);
            $post->categories()->attach($categories[1]->id);
        });

        $this->get('/posts/search?cats=1,2', $this->getHeaders())
            ->seeJson([
                'per_page' => 1,
                'last_page' => 1,
                'total' => 5
            ])
            ->seeJsonStructure([
                'data' => [
                    ['thumbnail']
                ], 'next_page_url', 'prev_page_url'
            ]);
    }

    /**
     * Delete post route test.
     *
     * @return void
     */
    public function testDelete()
    {
        $post = factory('App\Models\Post')->create();

        $this->delete('/posts/' . $post->id, [], $this->getheaders());

        $this->notSeeInDatabase('posts', [
            'id' => $post->id,
            'deleted_at' => null
        ]);
    }
}
