# composer install
# php artisan migrate --seed
FROM php:7-fpm-alpine

# Install mysql extension
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Download Composer
RUN curl -sS https://getcomposer.org/installer | php -- \
    --install-dir=/usr/local/bin \
    --filename=composer

# Setup workdir to project root
WORKDIR /var/www/aev-api

# Copy the application
COPY . .

# Create storage symbolic link
RUN ln -s ./public/uploads ./storage/app

# Create vendor directory
RUN mkdir -p ./vendor

# Fix storage permissions
RUN chown -R root:www-data ./storage ./vendor
RUN chmod -R ug+w ./storage ./vendor

# Change user
USER www-data

# Install depedencies
RUN composer install --no-plugins --no-scripts

# Listen to fastcgi requests
CMD ["php-fpm"]
