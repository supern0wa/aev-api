<?php

$factory->define(App\Models\Person::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'rg' => '2911220',
        'cpf' => '013.586.604-92',
        'email' => $faker->email,
        'phone' => $faker->tollFreePhoneNumber,
        'mobile' => $faker->tollFreePhoneNumber,
        'address_id' => factory('App\Models\Address')->create()->id
    ];
});
