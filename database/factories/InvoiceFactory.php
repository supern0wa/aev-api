<?php

$factory->define(App\Models\Invoice::class, function (Faker\Generator $faker) {
    return [
        'total' => 500.00,
        'discount' => 0,
        'person_id' => factory('App\Models\Person')->create()->id
    ];
});
