<?php

$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->catchPhrase,
        'slug' => $faker->slug,
        'excerpt' => $faker->sentence,
        'content' => $faker->text,
        'thumbnail_id' => factory('App\Models\Media', 'image')->create()->id
    ];
});

