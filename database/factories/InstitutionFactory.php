<?php

$factory->define(App\Models\Institution::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'content' => $faker->text,
        'location' => ['lat' => 0, 'lng' => 0],
        'address_id' => factory('App\Models\Address')->create()->id,
        'thumbnail_id' => factory('App\Models\Media', 'image')->create()->id
    ];
});
