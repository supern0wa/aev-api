<?php

$factory->define(App\Models\Volunteer::class, function (Faker\Generator $faker) {
    return [
        'civil_status' => 'single',
        'birthday' => $faker->date,
        'ocupation' => $faker->jobTitle,
        'religion' => $faker->randomElement(['Christian', 'Buddhist']),
        'reason' => $faker->paragraph,
        'suggestions' => $faker->paragraph,
        'abilities' => $faker->jobTitle,
        'days' => json_encode([
            'sun' => $faker->boolean(50),
            'mon' => $faker->boolean(50),
            'thu' => $faker->boolean(50),
            'wed' => $faker->boolean(50),
            'tue' => $faker->boolean(50),
            'fri' => $faker->boolean(50),
            'sat' => $faker->boolean(50)
        ]),
        'shifts' => json_encode([
            'first' => $faker->boolean(50),
            'second' => $faker->boolean(50)
        ]),
        'person_id' => factory('App\Models\Person')->create()->id
    ];
});
