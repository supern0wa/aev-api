<?php

$factory->define(App\Models\Feature::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->catchPhrase,
        'excerpt' => $faker->sentence,
        'link' => $faker->url,
        'order' => $faker->randomDigitNotNull,
        'thumbnail_id' => factory('App\Models\Media', 'image')->create()->id
    ];
});

