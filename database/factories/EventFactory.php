<?php

$factory->define(App\Models\Event::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->catchPhrase,
        'slug' => $faker->slug,
        'excerpt' => $faker->sentence,
        'content' => $faker->text,
        'date' => $faker->date,
        'institution_id' => 1,
        'thumbnail_id' => factory('App\Models\Media', 'image')->create()->id
    ];
});

