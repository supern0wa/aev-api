<?php

$factory->define(App\Models\Address::class, function (Faker\Generator $faker) {
    return [
        'street' => $faker->streetName,
        'neighbourhood' => $faker->streetSuffix,
        'number' => $faker->buildingNumber,
        'state' => 'SP',
        'city' => $faker->city,
        'zipcode' => '58025-650',
        'country_id' => 300
    ];
});
