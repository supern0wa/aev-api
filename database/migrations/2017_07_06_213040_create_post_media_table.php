<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order');
            $table->integer('post_id')->nullable()->unsigned();
            $table->integer('media_id')->nullable()->unsigned();
            $table->string('caption');

            $table->foreign('post_id')
                ->unique()
                ->references('id')->on('posts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('media_id')
                ->unique()
                ->references('id')->on('medias')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_media');
    }
}
