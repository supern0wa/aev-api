<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('civil_status', ['single', 'married', 'divorced', 'widowed'])->default('single');
            $table->date('birthday');
            $table->string('ocupation', 20);
            $table->string('religion', 20);
            $table->text('reason');
            $table->text('suggestions');
            $table->text('abilities');
            $table->text('days');
            $table->text('shifts');
            $table->integer('person_id')->nullable()->unsigned();
            
            $table->foreign('person_id')
                ->references('id')->on('people')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('volunteers');
    }
}
