<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('street', 100);
            $table->string('complement', 100)->nullable();
            $table->string('neighbourhood', 50);
            $table->string('zipcode', 15);
            $table->string('state', 40);
            $table->string('city', 100);
            $table->integer('number');
            $table->integer('country_id')->nullable()->unsigned();

            $table->foreign('country_id')
                ->unique()
                ->references('id')->on('countries')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses');
    }
}
