<?php

use Illuminate\Database\Seeder;
use App\Models\Institution;
use App\Models\Address;

class InstitutionSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return  void
	 */
	public function run()
	{
		// João Pessoa
		Institution::create([
			'name' => 'João Pessoa',
			'content' => '',
			'address_id' => Address::create([
				'street' => 'Rua Francisco Moura',
				'neighbourhood' => '13 de Maio',
				'city' => 'João Pessoa',
				'state' => 'Paraíba',
				'zipcode' => '58025-650',
				'number' => 312,
				'country_id' => 76
			])->id
		]);

		// Campina Grande
		Institution::create([
			'name' => 'Campina Grande',
			'content' => '',
			'address_id' => Address::create([
				'street' => 'Rua Francisco Moura',
				'neighbourhood' => '13 de Maio',
				'city' => 'João Pessoa',
				'state' => 'Paraíba',
				'zipcode' => '58025-650',
				'number' => 312,
				'country_id' => 76
			])->id
		]);
	}
}
