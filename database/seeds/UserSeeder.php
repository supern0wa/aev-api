<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Person;
use App\Models\Address;

class UserSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        // Jonathan
        User::create([
            'username' => 'jonathan',
            'password' => md5(123123),
            'person_id' => Person::create([
                'first_name' => 'Jonathan',
                'last_name' => 'Beltrão',
                'email' => 'jonathanbeltrao@gmail.com',
                'phone' => '(83) 9932-5757',
                'mobile' => '',
                'address_id' => Address::create([
                    'street' => 'Rua Francisco Moura',
                    'neighbourhood' => '13 de Maio',
                    'city' => 'João Pessoa',
                    'state' => 'PB',
                    'zipcode' => '58025-650',
                    'number' => 312,
                    'country_id' => 76
                ])->id
            ])->id
        ]);

        // Giseudo
        User::create([
            'username' => 'giseudo',
            'password' => md5(123123),
            'person_id' => Person::create([
                'first_name' => 'Giseudo',
                'last_name' => 'Oliveira',
                'email' => 'giseudo@gmail.com',
                'phone' => '(83) 98805-2880',
                'mobile' => '',
                'address_id' => Address::create([
                    'street' => 'Rua São Miguel',
                    'neighbourhood' => 'Varadouro',
                    'city' => 'João Pessoa',
                    'state' => 'PB',
                    'zipcode' => '58010-270',
                    'number' => 220,
                    'country_id' => 76
                ])->id
            ])->id
        ]);
    }
}
